package com.example.web_view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.gif.GifDrawable;

public class splash extends AppCompatActivity {
    private ImageView imageView;
    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        hideStatusBar();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context=this;

        imageView=findViewById(R.id.Giff);
        handleSplashScreen();





    }
    private void handleSplashScreen() {


        new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {

                    Intent intent= new Intent(splash.this, MainActivity.class);
                    startActivity(intent);
                }

        },5000);
    }
    @Override
    protected void onStop() {
        super.onStop();
        Log.d("CRITICAL", "onStop: ");
        finish();
    }
    public void hideStatusBar() {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}
